// -*- c++ -*-
// Generated by gmmproc 2.46.1 -- DO NOT MODIFY!
#ifndef _LIBGDAMM_BLOB_H
#define _LIBGDAMM_BLOB_H


#include <glibmm/ustring.h>
#include <sigc++/sigc++.h>

// -*- C++ -*- //

/* blob.h
 *
 * Copyright 2001      Free Software Foundation
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
 

#include <libgdamm/blobop.h>
#include <libgda/gda-value.h> // Seems like we need this

namespace Gnome
{

namespace Gda
{

/** This object is a base class for individual database providers which support BLOB types. 
 * It supports operations to read and write data in a BLOB.
 *
 * @ingroup DataHandlers
 */
class Blob
{
  public:
#ifndef DOXYGEN_SHOULD_SKIP_THIS
  typedef Blob CppObjectType;
  typedef GdaBlob BaseObjectType;
#endif /* DOXYGEN_SHOULD_SKIP_THIS */

  Blob(const Blob& other) noexcept;
  Blob& operator=(const Blob& other) noexcept;

  Blob(Blob&& other) noexcept;
  Blob& operator=(Blob&& other) noexcept;

  /** Get the GType for this class, for use with the underlying GObject type system.
   */
  static GType get_type() G_GNUC_CONST;

  Blob();

  explicit Blob(const GdaBlob* gobject); // always takes a copy

  ///Provides access to the underlying C instance.
  GdaBlob*       gobj()       { return &gobject_; }

  ///Provides access to the underlying C instance.
  const GdaBlob* gobj() const { return &gobject_; }

protected:
  GdaBlob gobject_;

private:
  
  
  public:
    
  /** correctly assigns @a op to @a blob
   * 
   * @param op A Gda::BlobOp object, or <tt>0</tt>.
   */
  void set_op(const Glib::RefPtr<BlobOp>& op);
    
  /** Converts all the non printable characters of blob->data into the \\xxx representation
   * where xxx is the octal representation of the byte, and the '\\' (backslash) character
   * is converted to "\\\\".
   * 
   * @param maxlen A maximum len used to truncate, or 0 for no maximum length.
   * @return A new string from @a blob.
   */
  Glib::ustring to_string(guint maxlen =  0) const;


};

} // namespace Gda

} // namespace Gnome


namespace Glib
{

/** @relates Gnome::Gda::Blob
 * @param object The C instance
 * @result A C++ instance that wraps this C instance.
 */
Gnome::Gda::Blob& wrap(GdaBlob* object);

/** @relates Gnome::Gda::Blob
 * @param object The C instance
 * @result A C++ instance that wraps this C instance.
 */
const Gnome::Gda::Blob& wrap(const GdaBlob* object);

#ifndef DOXYGEN_SHOULD_SKIP_THIS
template <>
class Value<Gnome::Gda::Blob> : public Glib::Value_Boxed<Gnome::Gda::Blob>
{};
#endif /* DOXYGEN_SHOULD_SKIP_THIS */

} // namespace Glib


#endif /* _LIBGDAMM_BLOB_H */


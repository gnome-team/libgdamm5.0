// Generated by gmmproc 2.46.1 -- DO NOT MODIFY!


#include <glibmm.h>

#include <libgdamm/datacomparator.h>
#include <libgdamm/private/datacomparator_p.h>


// -*- C++ -*- // this is for the .ccg, I realize gensig puts one in

/* datacomparator.cc
 * 
 * Copyright 2003 libgdamm Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <libgda/gda-data-comparator.h>

namespace Gnome
{

namespace Gda
{

void DataComparator::set_key_for_columns(const std::vector<int>& col_numbers)
{
  gda_data_comparator_set_key_columns(gobj(),
    Glib::ArrayHandler<int>::vector_to_array(col_numbers).data(),
    col_numbers.size());
}

} /* namespace Gda */

} /* namespace Gnome */


namespace
{


static gboolean DataComparator_signal_diff_computed_callback(GdaDataComparator* self, gpointer p0,void* data)
{
  using namespace Gnome::Gda;
  typedef sigc::slot< bool,Diff* > SlotType;

  auto obj = dynamic_cast<DataComparator*>(Glib::ObjectBase::_get_current_wrapper((GObject*) self));
  // Do not try to call a signal on a disassociated wrapper.
  if(obj)
  {
    try
    {
      if(const auto slot = Glib::SignalProxyNormal::data_to_slot(data))
        return static_cast<int>((*static_cast<SlotType*>(slot))((Diff*)(p0)
));
    }
    catch(...)
    {
       Glib::exception_handlers_invoke();
    }
  }

  typedef gboolean RType;
  return RType();
}

static gboolean DataComparator_signal_diff_computed_notify_callback(GdaDataComparator* self, gpointer p0, void* data)
{
  using namespace Gnome::Gda;
  typedef sigc::slot< void,Diff* > SlotType;

  auto obj = dynamic_cast<DataComparator*>(Glib::ObjectBase::_get_current_wrapper((GObject*) self));
  // Do not try to call a signal on a disassociated wrapper.
  if(obj)
  {
    try
    {
      if(const auto slot = Glib::SignalProxyNormal::data_to_slot(data))
        (*static_cast<SlotType*>(slot))((Diff*)(p0)
);
    }
    catch(...)
    {
      Glib::exception_handlers_invoke();
    }
  }

  typedef gboolean RType;
  return RType();
}

static const Glib::SignalProxyInfo DataComparator_signal_diff_computed_info =
{
  "diff-computed",
  (GCallback) &DataComparator_signal_diff_computed_callback,
  (GCallback) &DataComparator_signal_diff_computed_notify_callback
};


} // anonymous namespace


namespace Glib
{

Glib::RefPtr<Gnome::Gda::DataComparator> wrap(GdaDataComparator* object, bool take_copy)
{
  return Glib::RefPtr<Gnome::Gda::DataComparator>( dynamic_cast<Gnome::Gda::DataComparator*> (Glib::wrap_auto ((GObject*)(object), take_copy)) );
  //We use dynamic_cast<> in case of multiple inheritance.
}

} /* namespace Glib */


namespace Gnome
{

namespace Gda
{


/* The *_Class implementation: */

const Glib::Class& DataComparator_Class::init()
{
  if(!gtype_) // create the GType if necessary
  {
    // Glib::Class has to know the class init function to clone custom types.
    class_init_func_ = &DataComparator_Class::class_init_function;

    // This is actually just optimized away, apparently with no harm.
    // Make sure that the parent type has been created.
    //CppClassParent::CppObjectType::get_type();

    // Create the wrapper type, with the same class/instance size as the base type.
    register_derived_type(gda_data_comparator_get_type());

    // Add derived versions of interfaces, if the C type implements any interfaces:
  DataModel::add_interface(get_type());

  }

  return *this;
}


void DataComparator_Class::class_init_function(void* g_class, void* class_data)
{
  const auto klass = static_cast<BaseClassType*>(g_class);
  CppClassParent::class_init_function(klass, class_data);


}


Glib::ObjectBase* DataComparator_Class::wrap_new(GObject* object)
{
  return new DataComparator((GdaDataComparator*)object);
}


/* The implementation: */

GdaDataComparator* DataComparator::gobj_copy()
{
  reference();
  return gobj();
}

DataComparator::DataComparator(const Glib::ConstructParams& construct_params)
:
  Glib::Object(construct_params)
{

}

DataComparator::DataComparator(GdaDataComparator* castitem)
:
  Glib::Object((GObject*)(castitem))
{}


DataComparator::DataComparator(DataComparator&& src) noexcept
: Glib::Object(std::move(src))
  , DataModel(std::move(src))
{}

DataComparator& DataComparator::operator=(DataComparator&& src) noexcept
{
  Glib::Object::operator=(std::move(src));
  DataModel::operator=(std::move(src));
  return *this;
}

DataComparator::~DataComparator() noexcept
{}


DataComparator::CppClassType DataComparator::datacomparator_class_; // initialize static member

GType DataComparator::get_type()
{
  return datacomparator_class_.init().get_type();
}


GType DataComparator::get_base_type()
{
  return gda_data_comparator_get_type();
}


DataComparator::DataComparator(const Glib::RefPtr<DataModel>& old_model, const Glib::RefPtr<DataModel>& new_model)
:
  // Mark this class as non-derived to allow C++ vfuncs to be skipped.
  Glib::ObjectBase(0),
  Glib::Object(Glib::ConstructParams(datacomparator_class_.init(), "old_model", Glib::unwrap(old_model), "new_model", Glib::unwrap(new_model), static_cast<char*>(0)))
{
  

}

Glib::RefPtr<DataComparator> DataComparator::create(const Glib::RefPtr<DataModel>& old_model, const Glib::RefPtr<DataModel>& new_model)
{
  return Glib::RefPtr<DataComparator>( new DataComparator(old_model, new_model) );
}

bool DataComparator::compute_diff()
{
  GError* gerror = nullptr;
  bool retvalue = gda_data_comparator_compute_diff(gobj(), &(gerror));
  if(gerror)
    ::Glib::Error::throw_exception(gerror);
  return retvalue;
}

int DataComparator::get_n_diffs() const
{
  return gda_data_comparator_get_n_diffs(const_cast<GdaDataComparator*>(gobj()));
}

const Diff* DataComparator::get_diff(int pos)
{
  return gda_data_comparator_get_diff(gobj(), pos);
}


Glib::SignalProxy1< bool,Diff* > DataComparator::signal_diff_computed()
{
  return Glib::SignalProxy1< bool,Diff* >(this, &DataComparator_signal_diff_computed_info);
}


Glib::PropertyProxy< Glib::RefPtr<DataModel> > DataComparator::property_old_model() 
{
  return Glib::PropertyProxy< Glib::RefPtr<DataModel> >(this, "old-model");
}

Glib::PropertyProxy_ReadOnly< Glib::RefPtr<DataModel> > DataComparator::property_old_model() const
{
  return Glib::PropertyProxy_ReadOnly< Glib::RefPtr<DataModel> >(this, "old-model");
}

Glib::PropertyProxy< Glib::RefPtr<DataModel> > DataComparator::property_new_model() 
{
  return Glib::PropertyProxy< Glib::RefPtr<DataModel> >(this, "new-model");
}

Glib::PropertyProxy_ReadOnly< Glib::RefPtr<DataModel> > DataComparator::property_new_model() const
{
  return Glib::PropertyProxy_ReadOnly< Glib::RefPtr<DataModel> >(this, "new-model");
}


} // namespace Gda

} // namespace Gnome



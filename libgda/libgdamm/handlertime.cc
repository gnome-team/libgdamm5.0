// Generated by gmmproc 2.46.1 -- DO NOT MODIFY!


#include <glibmm.h>

#include <libgdamm/handlertime.h>
#include <libgdamm/private/handlertime_p.h>


// -*- C++ -*- // this is for the .ccg, I realize gensig puts one in

/* handlerstring.cc
 * 
 * Copyright 2003 libgdamm Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <libgdamm/handlerstring.h>

namespace Gnome
{


} /* namespace Gnome */


namespace
{
} // anonymous namespace


namespace Glib
{

Glib::RefPtr<Gnome::Gda::HandlerTime> wrap(GdaHandlerTime* object, bool take_copy)
{
  return Glib::RefPtr<Gnome::Gda::HandlerTime>( dynamic_cast<Gnome::Gda::HandlerTime*> (Glib::wrap_auto ((GObject*)(object), take_copy)) );
  //We use dynamic_cast<> in case of multiple inheritance.
}

} /* namespace Glib */


namespace Gnome
{

namespace Gda
{


/* The *_Class implementation: */

const Glib::Class& HandlerTime_Class::init()
{
  if(!gtype_) // create the GType if necessary
  {
    // Glib::Class has to know the class init function to clone custom types.
    class_init_func_ = &HandlerTime_Class::class_init_function;

    // This is actually just optimized away, apparently with no harm.
    // Make sure that the parent type has been created.
    //CppClassParent::CppObjectType::get_type();

    // Create the wrapper type, with the same class/instance size as the base type.
    register_derived_type(gda_handler_time_get_type());

    // Add derived versions of interfaces, if the C type implements any interfaces:
  DataHandler::add_interface(get_type());

  }

  return *this;
}


void HandlerTime_Class::class_init_function(void* g_class, void* class_data)
{
  const auto klass = static_cast<BaseClassType*>(g_class);
  CppClassParent::class_init_function(klass, class_data);


}


Glib::ObjectBase* HandlerTime_Class::wrap_new(GObject* object)
{
  return new HandlerTime((GdaHandlerTime*)object);
}


/* The implementation: */

GdaHandlerTime* HandlerTime::gobj_copy()
{
  reference();
  return gobj();
}

HandlerTime::HandlerTime(const Glib::ConstructParams& construct_params)
:
  Glib::Object(construct_params)
{

}

HandlerTime::HandlerTime(GdaHandlerTime* castitem)
:
  Glib::Object((GObject*)(castitem))
{}


HandlerTime::HandlerTime(HandlerTime&& src) noexcept
: Glib::Object(std::move(src))
  , DataHandler(std::move(src))
{}

HandlerTime& HandlerTime::operator=(HandlerTime&& src) noexcept
{
  Glib::Object::operator=(std::move(src));
  DataHandler::operator=(std::move(src));
  return *this;
}

HandlerTime::~HandlerTime() noexcept
{}


HandlerTime::CppClassType HandlerTime::handlertime_class_; // initialize static member

GType HandlerTime::get_type()
{
  return handlertime_class_.init().get_type();
}


GType HandlerTime::get_base_type()
{
  return gda_handler_time_get_type();
}


HandlerTime::HandlerTime()
:
  // Mark this class as non-derived to allow C++ vfuncs to be skipped.
  Glib::ObjectBase(0),
  Glib::Object(Glib::ConstructParams(handlertime_class_.init()))
{
  

}

Glib::RefPtr<HandlerTime> HandlerTime::create()
{
  return Glib::RefPtr<HandlerTime>( new HandlerTime() );
}


} // namespace Gda

} // namespace Gnome



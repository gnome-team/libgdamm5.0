// -*- c++ -*-
// Generated by gmmproc 2.46.1 -- DO NOT MODIFY!
#ifndef _LIBGDAMM_HANDLERBIN_H
#define _LIBGDAMM_HANDLERBIN_H


#include <glibmm/ustring.h>
#include <sigc++/sigc++.h>

// -*- C++ -*- //

/* handlerbin.h
 *
 * Copyright 2006 libgdamm Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or(at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <glibmm/object.h>
#include <libgdamm/datahandler.h>
#include <libgda/handlers/gda-handler-bin.h>


#ifndef DOXYGEN_SHOULD_SKIP_THIS
typedef struct _GdaHandlerBin GdaHandlerBin;
typedef struct _GdaHandlerBinClass GdaHandlerBinClass;
#endif /* DOXYGEN_SHOULD_SKIP_THIS */


#ifndef DOXYGEN_SHOULD_SKIP_THIS
namespace Gnome
{

namespace Gda
{ class HandlerBin_Class; } // namespace Gda

} // namespace Gnome
#endif //DOXYGEN_SHOULD_SKIP_THIS

namespace Gnome
{

namespace Gda
{

/** Default handler for binary values.
 *
 * @ingroup DataHandlers
 */

class HandlerBin : public Glib::Object,
                   public DataHandler
{
  
#ifndef DOXYGEN_SHOULD_SKIP_THIS

public:
  typedef HandlerBin CppObjectType;
  typedef HandlerBin_Class CppClassType;
  typedef GdaHandlerBin BaseObjectType;
  typedef GdaHandlerBinClass BaseClassType;

  // noncopyable
  HandlerBin(const HandlerBin&) = delete;
  HandlerBin& operator=(const HandlerBin&) = delete;

private:  friend class HandlerBin_Class;
  static CppClassType handlerbin_class_;

protected:
  explicit HandlerBin(const Glib::ConstructParams& construct_params);
  explicit HandlerBin(GdaHandlerBin* castitem);

#endif /* DOXYGEN_SHOULD_SKIP_THIS */

public:

  HandlerBin(HandlerBin&& src) noexcept;
  HandlerBin& operator=(HandlerBin&& src) noexcept;

  virtual ~HandlerBin() noexcept;

  /** Get the GType for this class, for use with the underlying GObject type system.
   */
  static GType get_type()      G_GNUC_CONST;

#ifndef DOXYGEN_SHOULD_SKIP_THIS


  static GType get_base_type() G_GNUC_CONST;
#endif

  ///Provides access to the underlying C GObject.
  GdaHandlerBin*       gobj()       { return reinterpret_cast<GdaHandlerBin*>(gobject_); }

  ///Provides access to the underlying C GObject.
  const GdaHandlerBin* gobj() const { return reinterpret_cast<GdaHandlerBin*>(gobject_); }

  ///Provides access to the underlying C instance. The caller is responsible for unrefing it. Use when directly setting fields in structs.
  GdaHandlerBin* gobj_copy();

private:

  
protected:

  // TODO: We cannot wrap gda_handler_bin_new_with_prov because this sets some
  // private fields. I guess these should be properties in libgda. armin.
  //   Maybe that is fixed now. murrayc.
 HandlerBin();

public:
  
  static Glib::RefPtr<HandlerBin> create();


public:

public:
  //C++ methods used to invoke GTK+ virtual functions:

protected:
  //GTK+ Virtual Functions (override these to change behaviour):

  //Default Signal Handlers::


};

} // namespace Gda
} // namespace Gnome


namespace Glib
{
  /** A Glib::wrap() method for this object.
   * 
   * @param object The C instance.
   * @param take_copy False if the result should take ownership of the C instance. True if it should take a new copy or ref.
   * @result A C++ instance that wraps this C instance.
   *
   * @relates Gnome::Gda::HandlerBin
   */
  Glib::RefPtr<Gnome::Gda::HandlerBin> wrap(GdaHandlerBin* object, bool take_copy = false);
}


#endif /* _LIBGDAMM_HANDLERBIN_H */


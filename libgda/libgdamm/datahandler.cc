// Generated by gmmproc 2.46.1 -- DO NOT MODIFY!


#include <glibmm.h>

#include <libgdamm/datahandler.h>
#include <libgdamm/private/datahandler_p.h>


// -*- C++ -*- // this is for the .ccg, I realize gensig puts one in

/* datahandler.cc
 * 
 * Copyright 2003 libgdamm Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <libgdamm/serverprovider.h>
#include <libgda/gda-data-model.h>
#include <libgda/gda-data-model-extra.h>
#include <libgda/gda-enum-types.h>


namespace Gnome
{

namespace Gda
{

} /* namespace Gda */

} /* namespace Gnome */


namespace
{
} // anonymous namespace


namespace Glib
{

Glib::RefPtr<Gnome::Gda::DataHandler> wrap(GdaDataHandler* object, bool take_copy)
{
  return Glib::RefPtr<Gnome::Gda::DataHandler>( dynamic_cast<Gnome::Gda::DataHandler*> (Glib::wrap_auto_interface<Gnome::Gda::DataHandler> ((GObject*)(object), take_copy)) );
  //We use dynamic_cast<> in case of multiple inheritance.
}

} // namespace Glib


namespace Gnome
{

namespace Gda
{


/* The *_Class implementation: */

const Glib::Interface_Class& DataHandler_Class::init()
{
  if(!gtype_) // create the GType if necessary
  {
    // Glib::Interface_Class has to know the interface init function
    // in order to add interfaces to implementing types.
    class_init_func_ = &DataHandler_Class::iface_init_function;

    // We can not derive from another interface, and it is not necessary anyway.
    gtype_ = gda_data_handler_get_type();
  }

  return *this;
}

void DataHandler_Class::iface_init_function(void* g_iface, void*)
{
  const auto klass = static_cast<BaseClassType*>(g_iface);

  //This is just to avoid an "unused variable" warning when there are no vfuncs or signal handlers to connect.
  //This is a temporary fix until I find out why I can not seem to derive a GtkFileChooser interface. murrayc
  g_assert(klass != nullptr); 


}


Glib::ObjectBase* DataHandler_Class::wrap_new(GObject* object)
{
  return new DataHandler((GdaDataHandler*)(object));
}


/* The implementation: */

DataHandler::DataHandler()
:
  Glib::Interface(datahandler_class_.init())
{}

DataHandler::DataHandler(GdaDataHandler* castitem)
:
  Glib::Interface((GObject*)(castitem))
{}

DataHandler::DataHandler(const Glib::Interface_Class& interface_class)
: Glib::Interface(interface_class)
{
}

DataHandler::DataHandler(DataHandler&& src) noexcept
: Glib::Interface(std::move(src))
{}

DataHandler& DataHandler::operator=(DataHandler&& src) noexcept
{
  Glib::Interface::operator=(std::move(src));
  return *this;
}

DataHandler::~DataHandler() noexcept
{}

// static
void DataHandler::add_interface(GType gtype_implementer)
{
  datahandler_class_.init().add_interface(gtype_implementer);
}

DataHandler::CppClassType DataHandler::datahandler_class_; // initialize static member

GType DataHandler::get_type()
{
  return datahandler_class_.init().get_type();
}


GType DataHandler::get_base_type()
{
  return gda_data_handler_get_type();
}


Glib::ustring DataHandler::get_sql_from_value(const Value& value) const
{
  return Glib::convert_return_gchar_ptr_to_ustring(gda_data_handler_get_sql_from_value(const_cast<GdaDataHandler*>(gobj()), (value).gobj()));
}

Glib::ustring DataHandler::get_str_from_value(const Value& value) const
{
  return Glib::convert_return_gchar_ptr_to_ustring(gda_data_handler_get_str_from_value(const_cast<GdaDataHandler*>(gobj()), (value).gobj()));
}

Value DataHandler::get_value_from_sql(const Glib::ustring& sql, GType type) const
{
  return Value(gda_data_handler_get_value_from_sql(const_cast<GdaDataHandler*>(gobj()), sql.c_str(), type));
}

Value DataHandler::get_value_from_str(const Glib::ustring& sql, GType type) const
{
  return Value(gda_data_handler_get_value_from_str(const_cast<GdaDataHandler*>(gobj()), sql.c_str(), type));
}

Value DataHandler::get_sane_init_value(GType type) const
{
  return Value(gda_data_handler_get_sane_init_value(const_cast<GdaDataHandler*>(gobj()), type));
}

bool DataHandler::accepts_g_type(GType type) const
{
  return gda_data_handler_accepts_g_type(const_cast<GdaDataHandler*>(gobj()), type);
}

Glib::ustring DataHandler::get_descr() const
{
  return Glib::convert_const_gchar_ptr_to_ustring(gda_data_handler_get_descr(const_cast<GdaDataHandler*>(gobj())));
}


} // namespace Gda

} // namespace Gnome



/* $Id: connectionevent.hg,v 1.2 2006/06/11 08:12:29 murrayc Exp $ */
// -*- C++ -*- //

/* connectionevent.h
 *
 * Copyright 2003 libgdamm Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <glibmm/object.h>
#include <libgda/gda-connection-event.h>

_DEFS(libgdamm,libgda)
_PINCLUDE(glibmm/private/object_p.h)

namespace Gnome
{

namespace Gda
{

_WRAP_ENUM(ConnectionEventCode, GdaConnectionEventCode)
_WRAP_ENUM(ConnectionEventType, GdaConnectionEventType)

/** Any event which has occurred on a Gda::Connection.
 * Events occuring on a connection are each represented as a ConnectionEvent object. 
 * Each Connection is responsible for keeping a list of past events; that list can be consulted using the Connection::get_events() function.
 *
 * @ingroup Connections
 */
class ConnectionEvent : public Glib::Object
{
  _CLASS_GOBJECT(ConnectionEvent, GdaConnectionEvent, GDA_CONNECTION_EVENT, Glib::Object, GObject)
  _IGNORE(gda_connection_event_free)
protected:

 _CTOR_DEFAULT

public:
  _WRAP_CREATE()

  _WRAP_METHOD(void set_event_type(ConnectionEventType type), gda_connection_event_set_event_type)
  _WRAP_METHOD(ConnectionEventType get_event_type() const, gda_connection_event_get_event_type)
  _WRAP_METHOD(Glib::ustring get_description() const, gda_connection_event_get_description)
  _WRAP_METHOD(void set_description(const Glib::ustring& description), gda_connection_event_set_description)
  _WRAP_METHOD(glong get_code() const, gda_connection_event_get_code)
  _WRAP_METHOD(void set_code(glong code), gda_connection_event_set_code)
  _WRAP_METHOD(ConnectionEventCode get_gda_code() const, gda_connection_event_get_gda_code)
  _WRAP_METHOD(void set_gda_code(ConnectionEventCode code), gda_connection_event_set_gda_code)
  _WRAP_METHOD(Glib::ustring get_source() const, gda_connection_event_get_source)
  _WRAP_METHOD(void set_source(const Glib::ustring& source), gda_connection_event_set_source)
  _WRAP_METHOD(Glib::ustring get_sqlstate() const, gda_connection_event_get_sqlstate)
  _WRAP_METHOD(void set_sqlstate(const Glib::ustring& sqlstate), gda_connection_event_set_sqlstate)

  _WRAP_PROPERTY("type", ConnectionEventType)
};

} // namespace Gda
} // namespace Gnome

